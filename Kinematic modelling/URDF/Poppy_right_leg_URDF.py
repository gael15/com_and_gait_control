#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 14:37:22 2021

@author: gael guetin
"""


# =============================================================================
# Poppy right leg URDF modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================
def MGD_r_leg(theta1, theta2, theta3, theta4, theta5):

    d1 = -0.0225
    d2 = -0.044
    d3 = 0.005
    d4 = -0.024
    d5 = -0.182
    d6 = -0.18
    d7 = 0.035
    
    #Calcul de TO1
    t1 = np.array([[1, 0, 0, d1],[0, 1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
   
    rotx = np.array([[1, 0, 0, 0],[0, 0, -1, 0],[0, 1, 0, 0],[0, 0, 0, 1]])
   
    rotz_theta1 = np.array([[cos(theta1), sin(theta1), 0, 0],[-sin(theta1), cos(theta1), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    T01 = np.dot(t1,np.dot(rotx,rotz_theta1))
    
    #Compute T12
    t2 = np.array([[1, 0, 0, d2],[0, 1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    t3 = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, d3],[0, 0, 0, 1]])
    
    roty_theta2 = np.array([[cos(theta2), 0, -sin(theta2), 0],[0, 1, 0, 0],[sin(theta2), 0, cos(theta2), 0],[0, 0, 0, 1]])
    
    t12 = np.dot(t2,np.dot(t3,roty_theta2))
    
    #Calcul de T23
    t4 = np.array([[1, 0, 0, 0],[0, 1, 0, d4],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    rotx_theta3 = np.array([[1, 0, 0, 0],[0, cos(theta3), sin(theta3), 0],[0, sin(theta3), cos(theta3), 0],[0, 0, 0, 1]])
    
    t23 = np.dot(t4,rotx_theta3)
    
    #Compute T34
    t5 = np.array([[1, 0, 0, 0],[0, 1, 0, d5],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    roty = np.array([[0, 0, 1, 0],[0, 1, 0, 0],[-1, 0, 0, 0],[0, 0, 0, 1]])
    
    rotz_theta4 = np.array([[cos(theta4), sin(theta4), 0, 0],[-sin(theta4), cos(theta4), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
   
    t34 = np.dot(t5,np.dot(roty,rotz_theta4))
    
    #Compute T45
    t6 = np.array([[1, 0, 0, 0],[0, 1, 0, d6],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    roty_m = np.array([[0, 0, -1, 0],[0, 1, 0, 0],[1, 0, 0, 0],[0, 0, 0, 1]])
    
    rotx_theta5 = np.array([[1, 0, 0, 0],[0, cos(theta5), sin(theta5), 0],[0, -sin(theta5), cos(theta5), 0],[0, 0, 0, 1]])
    
    t45 = np.dot(t6,np.dot(roty_m,rotx_theta5))
    
    T02 = np.dot(T01,t12)
    
    T03 = np.dot(T02,t23)
    
    T04 = np.dot(T03,t34)
    
    T05 = np.dot(T04,t45)

    O = np.array([[0],[d7],[0], [1]])
    
    OT = np.dot(T05,O)
    
    T = OT[0:3,:]
    
    return T,T05,T04,T03,T02,T01

if __name__ == '__main__' :
    theta1=0
    theta2=0
    theta3=0
    theta4=0
    theta5=0
    Xi,Rot_5,Rot_4,Rot_3,Rot_2,Rot_1  = MGD_r_leg(theta1,theta2,theta3,theta4,theta5)
    
    