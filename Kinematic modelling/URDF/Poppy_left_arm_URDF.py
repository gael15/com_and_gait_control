#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 11:54:59 2021

@author: gael guetin
"""
# =============================================================================
# Poppy left arm DH-modified modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================

def MGD_l (theta1, theta2, theta3, theta4):

    a1 = 0.0771
    a2 = 0.05
    a3 = 0.004
    a4 = 0.0284
    a5 = 0.03625
    a6 = 0.0185
    a7 = 0.11175
    a8 = -0.01
    a9 = 0.15
    
    x =(cos(theta2)*cos(theta4)+sin(theta2)*sin(theta3)*sin(theta4))*a9 + cos(theta2)*a7 + sin(theta2)*sin(theta3)*a8 + cos(theta2)*a5 + a4 + a1
    
    y=(-sin(theta1)*sin(theta2)*cos(theta4)+(sin(theta1)*cos(theta2)*sin(theta3)-cos(theta1)*cos(theta3))*sin(theta4))*a9 - sin(theta1)*sin(theta2)*a7 + (sin(theta1)*cos(theta2)*sin(theta3)-cos(theta1)*cos(theta3))*a8 - sin(theta1)*sin(theta2)*a5-cos(theta1)*a6+a2
    
    z=(cos(theta1)*sin(theta2)*cos(theta4)+(-cos(theta1)*cos(theta2)*sin(theta3)-sin(theta1)*cos(theta3))*sin(theta4))*a9 + cos(theta1)*sin(theta2)*a7 + (-cos(theta1)*cos(theta2)*sin(theta3)-sin(theta1)*cos(theta3))*a8 + cos(theta1)*sin(theta2)*a5 - sin(theta1)*a6 + a3
    
    X =np.array([[x], [y], [z]])

    return X

if __name__ == '__main__' :
  
    theta1 = 0
    theta2 = 0
    theta3 = 0
    theta4 = 0

    Xi=MGD_l(theta1, theta2, theta3, theta4)
