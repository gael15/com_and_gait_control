#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 15:01:10 2021

@author: gael guetin
"""
# =============================================================================
# Poppy abdomen URDF modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================

def MGD_abdomen(theta1, theta2, theta3, theta4, theta5):
    
    e1 = -0.017
    
    e2 = 0.061
    
    e3 = 0.05164
    
    e4 = 0.07985
    
    e5 = 0.0028

    #Computing of T01
    Ta = np.array([[1, 0, 0, 0],[0, 1, 0, e1],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Tb = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, e2],[0, 0, 0, 1]])
    
    Tc = np.array([[1, 0, 0, 0],[0, 0, -1, 0],[0, 1, 0, 0],[0, 0, 0, 1]])
    
    Td = np.array([[1, 0, 0, 0],[0, cos(theta1), sin(theta1), 0],[0, -sin(theta1), cos(theta1), 0],[0, 0, 0, 1]])
    
    #Computing of T12
    
    T12 = np.array([[cos(theta2), 0, sin(theta2), 0],[0, 1, 0, 0],[-sin(theta2), 0, cos(theta2), 0],[0, 0, 0, 1]])
    
    #Computing of T23
    
    Te = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, e3],[0, 0, 0, 1]])
    
    Tf = np.array([[cos(theta3), 0, sin(theta3), 0],[0, 1, 0, 0],[-sin(theta3), 0, cos(theta3), 0],[0, 0, 0, 1]])
    
    #Computing of T34
    
    Tg = np.array([[1, 0, 0, 0],[0, 1, 0, e4],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Th = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, e5],[0, 0, 0, 1]])
    
    Ti =  np.array([[1, 0, 0, 0],[0, cos(theta4), sin(theta4), 0],[0, -sin(theta4), cos(theta4), 0],[0, 0, 0, 1]])
    
    #Computing of T45
    
    T45 = np.array([[cos(theta5), -sin(theta5), 0, 0],[sin(theta5), cos(theta5), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])

    T01 = Ta@Tb@Tc@Td
    
    T23 = Te@Tf
    
    T34 =Tg@Th@Ti
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 = np.dot(T03,T34)
    
    T05 = np.dot(T04,T45)
  
    return T05,T04,T03,T02,T01

if __name__ == '__main__' :

    theta1 = 0
    theta2 = 0
    theta3 = 0
    theta4 = 0
    theta5 = 0
    
    Rot_5,Rot_4,Rot_3,Rot_2,Rot_1 = MGD_abdomen(theta1, theta2, theta3, theta4, theta5)
    

