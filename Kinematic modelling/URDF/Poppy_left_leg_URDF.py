#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 14:15:32 2021

@author: gael guetin
"""

# =============================================================================
# Poppy left leg URDF modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================
def MGD_l_leg(theta1, theta2, theta3, theta4,theta5):
    a0 = 0.0225417
    a1 = 0.0439986
    a2 = 0.005
    a3 = -0.024
    a4 = -0.182
    a5 = -0.18
    a6 = -0.035

#Compute T01
   
    Ta = np.array([[1, 0, 0, a0],[0, 1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Tb = np.array([[1, 0, 0, 0],[0, 0, -1, 0],[0, 1, 0, 0],[0, 0, 0, 1]])
    
    Tc = np.array([[cos(theta1), -sin(theta1), 0, 0],[sin(theta1), cos(theta1), 0, 0], [0, 0, 1, 0],[0, 0, 0, 1]])

#Compute T12

    Td = np.array([[1, 0, 0, a1],[0, 1, 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Te = np.array([[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 1, a2],[0, 0, 0, 1]])
    
    Tf =  np.array([[cos(theta2),0, sin(theta2),0],[ 0, 1, 0, 0 ], [-sin(theta2), 0, cos(theta2),0],[0, 0, 0, 1]])

#Compute T23

    Tg = np.array([[1, 0, 0, 0],[0, 1, 0, a3],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Th =  np.array([[1, 0, 0, 0], [0, cos(theta3), -sin(theta3), 0],[0, sin(theta3), cos(theta3),0],[0, 0, 0, 1]])
    
    
#Compute T34

    Ti = np.array([[1, 0, 0, 0],[0, 1, 0, a4],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Tj = np.array([[0, 0, 1, 0],[0, 1, 0, 0],[-1, 0, 0, 0],[0, 0, 0, 1]])
    
    Tk = np.array([[cos(theta4), -sin(theta4), 0, 0],[sin(theta4), cos(theta4), 0, 0], [0, 0, 1, 0],[0, 0, 0, 1]])
    
#Compute T45

    Tl = np.array([[1, 0, 0, 0],[0, 1, 0, a5],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    Tm = np.array([[0, 0, -1, 0],[0, 1, 0, 0],[1, 0, 0, 0],[0, 0, 0, 1]])
    
    Tn = np.array([[1, 0, 0, 0], [0, cos(theta5), -sin(theta5), 0],[0, sin(theta5), cos(theta5),0],[0, 0, 0, 1]])
    
#Compute FO

    O6 = np.array([[0],[a6],[0], [1]])
    
    t1 = np.dot(Ta,Tb)
    
    T01 = np.dot(t1,Tc)
    
    t2 =np.dot(Td,Te)
    
    T12 = np.dot(t2,Tf)
     
    T23 = np.dot(Tg,Th)
    
    t3 = np.dot(Ti,Tj)
      
    T34 = np.dot(t3,Tk)
    
    t4 = np.dot(Tl,Tm)
    
    T45 = np.dot(t4, Tn)
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 = np.dot(T03,T34)
    
    T05 = np.dot(T04,T45)
    
    OT = np.dot(T05,O6)
    
    Xi = OT[0:3,:]
 
    return Xi,T05,T04,T03,T02,T01

if __name__ == '__main__' :
    
    theta1=0
    theta2=0
    theta3=0
    theta4=0
    theta5=0
    Xi,Rot_5,Rot_4,Rot_3,Rot_2,Rot_1  = MGD_l_leg(theta1,theta2,theta3,theta4,theta5)
    
