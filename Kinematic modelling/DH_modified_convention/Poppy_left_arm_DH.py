#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 13:48:47 2021

@author: gael guetin
"""


# =============================================================================
# Poppy left arm DH-modified modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================

def MGD_l_arm (theta1, theta2, theta3, theta4):
    a0 = 0.05
    a1 = 0.004
    a3 = 0.0185 - 0.01
    r1 = 0.0771
    r3 = 0.0284 + 0.03625 + 0.11175
    a5 = 0.15

    T01 = np.array([[-sin(theta1), -cos(theta1), 0, a0],[cos(theta1), -sin(theta1), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[-cos(theta2), sin(theta2), 0, a1],[0, 0, 1, 0],[sin(theta2), cos(theta2), 0, 0],[0, 0, 0, 1]])
    
    T23 = np.array([[sin(theta3), cos(theta3), 0, 0], [0, 0, 1, r3], [cos(theta3), -sin(theta3), 0, 0],[0, 0, 0, 1]])
    
    T34 = np.array([[sin(theta4), cos(theta4), 0, a3], [0, 0, 1, 0], [cos(theta4), -sin(theta4), 0, 0],[0, 0, 0, 1]])
  
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 = np.dot(T03,T34)
    
    O = np.array([[a5],[0],[0], [1]])
    
    OT = np.dot(T04,O)
    
    T = OT[0:3,:]
    
    X = np.zeros((3,1))
    
    X = np.array([T[2],T[1],-T[0]])
    
    return T,T04,T03,T02,T01

if __name__ == '__main__' :
    theta1=0
    theta2=0
    theta3=0
    theta4=0
    Xi,Rot_4,Rot_3,Rot_2,Rot_1  = MGD_l_arm(theta1,theta2,theta3,theta4)
    
    print(Xi)