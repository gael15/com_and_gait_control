#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 14:14:55 2021

@author: gael guetin
"""


# =============================================================================
# Poppy left leg DH-modified modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================
def MGD_l_leg(theta1, theta2, theta3, theta4,theta5):
  
    a5 = -0.035
    
    a0 =0.0225417
    a1 =0.04399986
    a3 = -0.182
    a4 =-0.18
    r1 =-0.005
    r2 =0.024
    
    T01 = np.array([[cos(theta1), -sin(theta1), 0, a0],[sin(theta1), cos(theta1), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[-sin(theta2), -cos(theta2), 0, a1], [0, 0, 1 , r2], [-cos(theta2), sin(theta2), 0, 0], [0, 0, 0, 1]])
        
    T23 = np.array([[sin(theta3), -cos(theta3), 0, 0], [0, 0, 1 , 0], [cos(theta3), sin(theta3), 0, 0], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta4), -sin(theta4), 0, a3],[sin(theta4), cos(theta4), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
        
    T45 =np.array([[cos(theta5), -sin(theta5), 0, a4],[sin(theta5), cos(theta5), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
   
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45)
    
    pose = np.array([[a5],[0],[0], [1]])
    
    X = np.dot(T05,pose)
    
    Xi = X[0:3,:]
    
    return Xi,T05,T04,T03,T02,T01

if __name__ == '__main__' :
    theta1=0
    theta2=0
    theta3=0
    theta4=0
    theta5=0
    Xi,Rot_5,Rot_4,Rot_3,Rot_2,Rot_1  = MGD_l_leg(theta1,theta2,theta3,theta4,theta5)
    print(Xi)
    
    