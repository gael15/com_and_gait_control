#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 14:52:49 2021

@author: gael guetin
"""

# =============================================================================
# Poppy abdomen DH-modified modelling
# =============================================================================


# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import matplotlib.pyplot as plt

# =============================================================================
# Direct kinematic model
# =============================================================================
def MGD_abdomen(theta1, theta2, theta3, theta4, theta5):
    
    theta=np.zeros(5)
    
    theta[0] = theta1
    theta[1] = theta2
    theta[2] = theta3
    theta[3] = theta4
    theta[4] = theta5
    
    theta = np.array([theta[0], theta[1], theta[2], theta[3], theta[4]])
    
    alpha = np.array([0, pi/2, -pi/2, pi/2, pi/2])
    
    a = np.array([0, 0.05164+0.017+0.0028, 0, 0, 0])
    
    r = np.array([0, 0, 0, 0.07985+0.061, 0])
    
    T01 = np.array([[cos(theta[0]), -sin(theta[0]), 0, a[0]], [cos(alpha[0])*sin(theta[0]), cos(alpha[0])*cos(theta[0]), -sin(alpha[0]) , -r[0]*sin(alpha[0])], [sin(alpha[0])*sin(theta[0]), sin(alpha[0])*cos(theta[0]), cos(alpha[0]), r[0]*cos(alpha[0])], [0, 0, 0, 1]])
    
    T12 = np.array([[cos(theta[1]), -sin(theta[1]), 0, a[1]], [cos(alpha[1])*sin(theta[1]), cos(alpha[1])*cos(theta[1]), -sin(alpha[1]) , -r[1]*sin(alpha[1])], [sin(alpha[1])*sin(theta[1]), sin(alpha[1])*cos(theta[1]), cos(alpha[1]), r[1]*cos(alpha[1])], [0, 0, 0, 1]])
        
    T23 = np.array([[cos(theta[2]), -sin(theta[2]), 0, a[2]], [cos(alpha[2])*sin(theta[2]), cos(alpha[2])*cos(theta[2]), -sin(alpha[2]) , -r[2]*sin(alpha[2])], [sin(alpha[2])*sin(theta[2]), sin(alpha[2])*cos(theta[2]), cos(alpha[2]), r[2]*cos(alpha[2])], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta[3]), -sin(theta[3]), 0, a[3]], [cos(alpha[3])*sin(theta[3]), cos(alpha[3])*cos(theta[3]), -sin(alpha[3]) , -r[3]*sin(alpha[3])], [sin(alpha[3])*sin(theta[3]), sin(alpha[3])*cos(theta[3]), cos(alpha[3]), r[3]*cos(alpha[3])], [0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta[4]), -sin(theta[4]), 0, a[4]], [cos(alpha[4])*sin(theta[4]), cos(alpha[4])*cos(theta[4]), -sin(alpha[4]) , -r[4]*sin(alpha[4])], [sin(alpha[4])*sin(theta[4]), sin(alpha[4])*cos(theta[4]), cos(alpha[4]), r[4]*cos(alpha[4])], [0, 0, 0, 1]])
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45)
 
    return T05,T04,T03,T02,T01

if __name__ == '__main__' :
    theta1=0
    theta2=0
    theta3=0
    theta4=0
    theta5=0
    Rot_5,Rot_4,Rot_3,Rot_2,Rot_1 = MGD_abdomen(theta1, theta2, theta3, theta4, theta5)
    print(Rot_5)
    
