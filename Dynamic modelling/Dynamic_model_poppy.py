#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 11:29:54 2022

@authors: Gael Guetin & Zingbeu Nimboh
"""

# =============================================================================
# Dynamic Modelling of robot Poppy
# =============================================================================

# =============================================================================
# 1st part : Dynamic model : Recursive method Newton-Euler
# =============================================================================

# =============================================================================
# Librairies
# =============================================================================
from math import *
import numpy as np
import sympy as sp
from sympy import symbols,Pow,sin,cos, gcd
import matplotlib.pyplot as plt
from scipy.linalg import lu_factor
# =============================================================================
# Left leg:  forward and backward
# =============================================================================

def left_leg(theta1, theta2, theta3, theta4, theta5, qp1, qp2, qp3, qp4, qp5, qpp1, qpp2, qpp3, qpp4, qpp5):
    
    # =============================================================================
    # Forward propagation
    # =============================================================================

    ####Kinematic model
    
    a0 =0.0225417
    
    a1 =0.04399986
    
    a3 = -0.182
    
    a4 =-0.18
    
    r1 =-0.005
    
    r2 =0.024
    
    T01 = np.array([[cos(theta1), -sin(theta1), 0, a0],[sin(theta1), cos(theta1), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])

    T12 = np.array([[-sin(theta2), -cos(theta2), 0, a1], [0, 0, 1 , r2], [-cos(theta2), sin(theta2), 0, 0], [0, 0, 0, 1]])
        
    T23 = np.array([[sin(theta3), -cos(theta3), 0, 0], [0, 0, 1 , 0], [cos(theta3), sin(theta3), 0, 0], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta4), -sin(theta4), 0, a3],[sin(theta4), cos(theta4), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
        
    T45 =np.array([[cos(theta5), -sin(theta5), 0, a4],[sin(theta5), cos(theta5), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
   
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45) 
    
    ##Extraction of rotation matrix
    R01 = T01[:3,:3]
    
    R02 = T02[:3,:3]
    
    R03 = T03[:3,:3]
    
    R04 = T04[:3,:3]
    
    R05 = T05[:3,:3]
    
   ####Length of segments and lengths of segments considering CoM
    
    r11 = np.array([[ 0.00336],[0.00738],[0.00498]])
    
    rc11 = np.array([[1.68e-3], [0.0003854], [0.03358]])
    
    r22 = np.array([[0.0016674],[-0.001541],[0.007076]])
        
    rc22 = np.array([[8.337e-4], [-7.705e-4], [3.538e-3]])
    
    r33 = np.array([[-0.006422],[0.011598],[0.10644]])
    
    rc33 = np.array([[-3.211e-3], [5.799e-3], [5.322e-2]])
    
    r44 = np.array([[0.002584],[-0.015828],[-0.09348]])
    
    rc44 = np.array([[1.292e-3], [-7.914e-3], [-4.674e-2]])
    
    r55 = np.array([[-0.0214],[0.0015032],[-0.004686]])
    
    rc55 = np.array([[-1.070e-2], [7.516e-4], [-2.343e-3]])
    
    ####Angular velocities and accelerations
    g = 9.81
    
    wp01 = np.array([[0],[0],[qp1]])
  
    wp02 = np.array([[0],[0],[qp1+qp2]])
    
    wp03 = np.array([[0],[0],[qp1+qp2+qp3]])
    
    wp04 = np.array([[0],[0],[qp1+qp2+qp3+qp4]])
    
    wp05 = np.array([[0],[0],[qp1+qp2+qp3+qp4+qp5]])
    
    wpp01 = np.array([[0],[0],[qpp1]])
    
    wpp02 = np.array([[0],[0],[qpp1+qpp2]])
    
    wpp03 = np.array([[0],[0],[qpp1+qpp2+qpp3]])
    
    wpp04 = np.array([[0],[0],[qpp1+qpp2+qpp3+qpp4]])
    
    wpp05 = np.array([[0],[0],[qpp1+qpp2+qpp3+qpp4+qpp5]])

    
    #1st segment 
        
        #Center of graviy velocity and acceleration
      
    Vp00 = np.array([[0],[-g],[0]])
    
    rc01= R01@rc11
   
    Vc01 = np.cross(wp01,rc01,axis=0)
    
    Vpc01 = Vp00 + np.cross(wpp01,rc01,axis=0) + np.cross(wp01,Vc01,axis=0)
        
        #Link velocity and acceleration
        
    r01= R01@r11
    
    V01 =  np.cross(wp01,r01,axis=0)
    
    Vp01 =  Vp00 + np.cross(wpp01,r01,axis=0) + np.cross(wp01,V01,axis=0)
    
    #2nd segment
        #Center of graviy velocity and acceleration
   
    rc02= r01 + R02@rc22
    
    Vc02 = V01 + np.cross(wp02,rc02,axis=0)
    
    Vpc02 = Vp01 + np.cross(wpp02,rc02,axis=0) + np.cross(wp02,Vc02,axis=0)
    
        #Link velocity and acceleration
        
    r02= r01 + R02@r22 
    
    V02 =  V01 + np.cross(wp02,r02,axis=0)
    
    Vp02 =  Vp01 + np.cross(wpp02,r02,axis=0) + np.cross(wp02,V02,axis=0)
    
    #3th segment
    
        #Center of graviy velocity and acceleration
   
    rc03= r02 + R03@rc33
    
    Vc03 = V02 + np.cross(wp03,rc03,axis=0)
    
    Vpc03 = Vp02 + np.cross(wpp03,rc03,axis=0) + np.cross(wp03,Vc03,axis=0)
    
        #Link velocity and acceleration
        
    r03= r02 + R03@r33 
    
    V03 =  V02 + np.cross(wp03,r03,axis=0)
    
    Vp03 =  Vp02 + np.cross(wpp03,r03,axis=0) + np.cross(wp03,V03,axis=0)
    
    #4th segment
    
        #Center of graviy velocity and acceleration
   
    rc04= r03 + R04@rc44
    
    Vc04 = V03 + np.cross(wp04,rc04,axis=0)
    
    Vpc04 = Vp03 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r04= r03 + R04@r44 
    
    V04 =  V03 + np.cross(wp04,r04,axis=0)
    
    Vp04 =  Vp03 + np.cross(wpp04,r04,axis=0) + np.cross(wp04,V04,axis=0)
    
    #5th segment
    
        #Center of graviy velocity and acceleration
   
    rc05= r04 + R04@rc44
    
    Vc05 = V04 + np.cross(wp05,rc05,axis=0)
    
    Vpc05 = Vp04 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r05= r04 + R04@r44 
    
    V05 =  V04 + np.cross(wp05,r05,axis=0)
    
    Vp05 =  Vp04 + np.cross(wpp05,r05,axis=0) + np.cross(wp05,V05,axis=0)
    
    # =============================================================================
    # Backward propagation
    # =============================================================================
        
    #5th segment
    
        ##Inertia matrix and link mass
        
    J5 = np.array([[8.1463e-5, -1.7921e-7, 2.4259e-6],[0, 8.8658e-5, -3.2353e-6],[0, 0, 1.7963e-5]])
    
    m5 = 4.683e-2

    F65 = np.zeros((3,3)) #Absence of force on the effector
    
    F54 = m5*Vp05
    
    R5 = rc05 - r05
    
    A5 = J5@wp05
    
    ###Torque value on 5th segment
    τ54 = -np.cross(F54,R5,axis=0)+J5@wpp05  + np.cross(wp05,A5,axis=0)
   
    #4th segment
    
        ##Inertia matrix and link mass
        
    J4 = np.array([[0.000397, 5.5011e-7, 0],[0, 3.9808e-5, 0],[-1.7326e-5, -3.5214e-9, 0.0003936]])
  
    m4 = 1.156e-1
    
    R4 = rc04 - r04
    
    r4 =-r05 -rc04
    
    A4 = J4@wp04
    
    F43 = m4*Vp04 + F54
    
    ###Torque value on 4th segment
    τ43 =   τ54 - np.cross(F43,R4,axis=0) + np.cross(F54,r4,axis=0) + J4@wpp04 + np.cross(wp04,A4,axis=0)
    

    #3th segment
    
        ##Inertia matrix and link mass
        
    J3 = np.array([[0.000345, 4.74e-5, -2.2e-6],[0, 5.1165e-5, -3.39e-6],[0, 0, 0.0003577]])
       
    m3 = 1.149e-1

    R3 = rc03 - r03
    
    r3 =-r04 -rc03
    
    A3 = J3@wp03
    
    F32 = m3*Vp03 + F43
    
    ###Torque value on 3th segment
    τ32 =   τ43 - np.cross(F32,R3,axis=0) + np.cross(F43,r3,axis=0) + J3@wpp03 + np.cross(wp03,A3,axis=0)
    
    
    #2nd segment
    
        ##Inertia matrix and link mass
        
    J2 = np.array([[2.9408e-5, 2.4022e-7, 1.1453e-8],[0, 3.598e-7, 0],[0, 7.9756e-7, 2.292e-5]])
       
    m2 = 8.317e-2

    R2 = rc02 - r02
    
    r2 =-r03 -rc02
    
    A2 = J2@wp02
    
    F21 = m2*Vp02 + F32
    
    ###Torque value on 2th segment
    τ21 = τ32 - np.cross(F21,R2,axis=0) + np.cross(F32,r2,axis=0) + J2@wpp02 + np.cross(wp02,A2,axis=0)
   
    #1st segment
    
        ##Inertia matrix and link mass
        
    J1 = np.array([[2.8079e-5, 1.2308e-6, -6.6239e-7],[0, -1.446e-6, 2.1594e-5],[0, 0, 2.1594e-5]]) 
       
    m1 = 8.438e-2

    R1 = rc01 - r01
    
    r1 =-r02 -rc01
    
    A1 = J1@wp01
    
    F10 = m1*Vp01 + F21
    
    ###Torque value on 1st segment
    τ10 = τ21 - np.cross(F10,R1,axis=0) + np.cross(F21,r1,axis=0) + J1@wpp01 + np.cross(wp01,A1,axis=0)
   
    ###Matrix of torques
    a = np.concatenate((τ10,τ21))
     
    b = np.concatenate((a,τ32))
       
    c = np.concatenate((b,τ43))      
                   
    d = np.concatenate((c,τ54))
    
    return d
 

# =============================================================================
# right leg:  forward and backward
# =============================================================================

def right_leg(theta6, theta7, theta8, theta9, theta10,qp6, qp7, qp8, qp9, qp10, qpp6, qpp7, qpp8, qpp9, qpp10):
    
    # =============================================================================
    # Forward propagation
    # =============================================================================

    ####Kinematic model
    a0 =0.0225417
    
    a1 =0.04399986
    
    a3 = 0.182
    
    a4 =0.18
    
    r1 =-0.005
    
    r2 =-0.024
      
    T01 = np.array([[cos(theta6), -sin(theta6), 0, a0],[sin(theta6), cos(theta6), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[sin(theta7), -cos(theta7), 0, a1], [0, 0, -1 , r2], [cos(theta7), sin(theta7), 0, 0], [0, 0, 0, 1]])
        
    T23 = np.array([[-sin(theta8), -cos(theta8), 0, 0], [0, 0, 1 , 0], [-cos(theta8), sin(theta8), 0, 0], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta9), -sin(theta9), 0, a3],[sin(theta9), cos(theta9), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta10), -sin(theta10), 0, a4],[sin(theta10), cos(theta10), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])

    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45) 
    
    ##Extraction of rotation matrix
    R01 = T01[:3,:3]
    
    R02 = T02[:3,:3]
    
    R03 = T03[:3,:3]
    
    R04 = T04[:3,:3]
    
    R05 = T05[:3,:3]
    
   ####Length of segments and lengths of segments considering CoM
    
    r11 = np.array([[ 0.00336],[0.0002102],[-0.03368]])
    
    rc11 = np.array([[1.678e-3], [1.051e-4], [-1.684e-2]])
   
    r22 = np.array([[0.0016678],[0.0012258],[0.007142]])
        
    rc22 = np.array([[8.339e-4], [6.129e-4], [3.571e-3]])
    
    r33 = np.array([[-0.006792],[0.01195],[-0.10366]])
    
    rc33 = np.array([[-3.396e-3], [5.975e-3], [-5.183e-2]])
 
    r44 = np.array([[-0.002706],[-0.015722],[-0.09354]])
    
    rc44 = np.array([[-1.353e-3], [-7.861e-3], [-4.677e-2]])
   
    r55 = np.array([[-0.02146],[-0.001252],[-0.004578]])
  
    rc55 = np.array([[-1.073e-2], [-6.26e-4], [-2.289e-3]])
    
    ####Angular velocities and accelerations
    g = 9.81
    
    wp01 = np.array([[0],[0],[qp6]])
  
    wp02 = np.array([[0],[0],[qp6+qp7]])
    
    wp03 = np.array([[0],[0],[qp6+qp7+qp8]])
    
    wp04 = np.array([[0],[0],[qp6+qp7+qp8+qp9]])
    
    wp05 = np.array([[0],[0],[qp6+qp7+qp8+qp9+qp10]])
    
    wpp01 = np.array([[0],[0],[qpp6]])
    
    wpp02 = np.array([[0],[0],[qpp6+qpp7]])
    
    wpp03 = np.array([[0],[0],[qpp6+qpp7+qpp8]])
    
    wpp04 = np.array([[0],[0],[qpp6+qpp7+qpp8+qpp9]])
    
    wpp05 = np.array([[0],[0],[qpp6+qpp7+qpp8+qpp9+qpp10]])
    
    wp = np.concatenate((wp01,wp02,wp03,wp04,wp05),axis=0)
    
    wpp = np.concatenate((wpp01,wpp02,wpp03,wpp04,wpp05),axis=0)
    
    r = np.concatenate((r11,r22,r33,r44,r55),axis=0)
    
    rc = np.concatenate((rc11,rc22,rc33,rc44,rc55),axis=0)
    
    #1st segment : 
        
        #Center of graviy velocity and acceleration
      
    Vp00 = np.array([[0],[-g],[0]])
    
    rc01= R01@rc11
   
    Vc01 = np.cross(wp01,rc01,axis=0)
    
    Vpc01 = Vp00 + np.cross(wpp01,rc01,axis=0) + np.cross(wp01,Vc01,axis=0)
        
        #Link velocity and acceleration
        
    r01= R01@r11
    
    V01 =  np.cross(wp01,r01,axis=0)
    
    Vp01 =  Vp00 + np.cross(wpp01,r01,axis=0) + np.cross(wp01,V01,axis=0)
    
    #2nd segment
        #Center of graviy velocity and acceleration
   
    rc02= r01 + R02@rc22
    
    Vc02 = V01 + np.cross(wp02,rc02,axis=0)
    
    Vpc02 = Vp01 + np.cross(wpp02,rc02,axis=0) + np.cross(wp02,Vc02,axis=0)
    
        #Link velocity and acceleration
        
    r02= r01 + R02@r22 
    
    V02 =  V01 + np.cross(wp02,r02,axis=0)
    
    Vp02 =  Vp01 + np.cross(wpp02,r02,axis=0) + np.cross(wp02,V02,axis=0)
    
    #3th segment
    
        #Center of graviy velocity and acceleration
   
    rc03= r02 + R03@rc33
    
    Vc03 = V02 + np.cross(wp03,rc03,axis=0)
    
    Vpc03 = Vp02 + np.cross(wpp03,rc03,axis=0) + np.cross(wp03,Vc03,axis=0)
    
        #Link velocity and acceleration
        
    r03= r02 + R03@r33 
    
    V03 =  V02 + np.cross(wp03,r03,axis=0)
    
    Vp03 =  Vp02 + np.cross(wpp03,r03,axis=0) + np.cross(wp03,V03,axis=0)
    
    #4th segment
    
        #Center of graviy velocity and acceleration
   
    rc04= r03 + R04@rc44
    
    Vc04 = V03 + np.cross(wp04,rc04,axis=0)
    
    Vpc04 = Vp03 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r04= r03 + R04@r44
    
    V04 =  V03 + np.cross(wp04,r04,axis=0)
    
    Vp04 =  Vp03 + np.cross(wpp04,r04,axis=0) + np.cross(wp04,V04,axis=0)
    
    #5th segment
    
        #Center of graviy velocity and acceleration
   
    rc05= r04 + R04@rc44
    
    Vc05 = V04 + np.cross(wp05,rc05,axis=0)
    
    Vpc05 = Vp04 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r05= r04 + R04@r44
    
    V05 =  V04 + np.cross(wp05,r05,axis=0)
    
    Vp05 =  Vp04 + np.cross(wpp05,r05,axis=0) + np.cross(wp05,V05,axis=0)
    
    # =============================================================================
    # Backward propagation
    # =============================================================================
        
    #5th segment
    
        ##Inertia matrix and link mass
        
    J5 = np.array([[8.144e-5, 0, 0],[-3.2302e-6, 8.8648e-5, 0],[-2.4206e-6, 1.7918e-7, 1.7946e-5]])
    
    m5 = 4.679e-2

    F65 = np.zeros((3,3)) #Absence of force on the effector
    
    F54 = m5*Vp05
    
    R5 = rc05 - r05
    
    A5 = J5@wp05
    
    ###Torque value on 5th segment
    τ54 = -np.cross(F54,R5,axis=0) + J5@wpp05 + np.cross(wp05,A5,axis=0)
    
    #4th segment
    
        ##Inertia matrix and link mass
        
    J4 = np.array([[0.000397, 1.329e-8, -1.7326e-5],[0, 3.981e-5, 0],[0, -5.8972e-7, 0.0003936]])
    
    m4 = 1.156e-1
    
    R4 = rc04 - r04
    
    r4 =-r05 -rc04
    
    A4 = J4@wp04
    
    F43 = m4*Vp04 + F54
    
    ###Torque value on 4th segment
    τ43 =   τ54 - np.cross(F43,R4,axis=0) + np.cross(F54,r4,axis=0) + J4@wpp04 + np.cross(wp04,A4,axis=0)
    
    #3th segment
    
        ##Inertia matrix and link mass
        
    J3 = np.array([[0.0003577, 0, 0],[-5.5406e-6, 5.1461e-5, 0],[2.19e-6, -4.7132e-5, 0.00037]])
       
    m3 = 1.164e-1

    R3 = rc03 - r03
    
    r3 =-r04 -rc03
    
    A3 = J3@wp03
    
    F32 = m3*Vp03 + F43
    
    ###Torque value on 3th segment
    τ32 =   τ43 - np.cross(F32,R3,axis=0) + np.cross(F43,r3,axis=0) + J3@wpp03 + np.cross(wp03,A3,axis=0)
     
    #2nd segment
    
        ##Inertia matrix and link mass
        
    J2 = np.array([[2.94e-5, 7.9756e-7, 0],[0, 3.598e-5, 0],[-1.199e-8, -2.4e-7, 2.292e-5]])
       
    m2 = 8.317e-2

    R2 = rc02 - r02
    
    r2 =-r03 -rc02
    
    A2 = J2@wp02
    
    F21 = m2*Vp02 + F32
    
    ###Torque value on 2th segment
    τ21 = τ32 - np.cross(F21,R2,axis=0) + np.cross(F32,r2,axis=0) + J2@wpp02 + np.cross(wp02,A2,axis=0)
    
    #1st segment
    
        ##Inertia matrix and link mass
        
    J1 = np.array([[2.808e-5, 0, 0],[-1.446e-6, 2.8338e-5, 0],[6.624e-7, -1.23e-6, 2.1593e-5]])
       
    m1 = 8.438e-2

    R1 = rc01 - r01
    
    r1 =-r02 -rc01
    
    A1 = J1@wp01
    
    F10 = m1*Vp01 + F21
    
    ###Torque value on 1st segment
    τ10 = τ21 - np.cross(F10,R1,axis=0) + np.cross(F21,r1,axis=0) + J1@wpp01 + np.cross(wp01,A1,axis=0)

    ###Matrix of torques
    a = np.concatenate((τ10,τ21))
     
    b = np.concatenate((a,τ32))
       
    c = np.concatenate((b,τ43))      
                   
    d = np.concatenate((c,τ54))
    
    
    return d

# =============================================================================
# abdomen :  forward and backward
# =============================================================================

def abdomen(theta11, theta12, theta13, theta14, theta15, qp11, qp12, qp13, qp14, qp15, qpp11, qpp12, qpp13, qpp14, qpp15):
    
    # =============================================================================
    # Forward propagation
    # =============================================================================

    ####Kinematic model
    
    a1 =0.05164+0.017+0.0028
    
    r4 =0.07985+0.061
    
    T01 = np.array([[cos(theta11), -sin(theta11), 0, 0],[sin(theta11), cos(theta11), 0, 0],[0, 0, 1, 0],[0, 0, 0, 1]])
    
    T12 = np.array([[cos(theta12), -sin(theta12), 0, a1], [0, 0, 1 , 0], [sin(theta12), cos(theta12), 0, 0], [0, 0, 0, 1]])
        
    T23 = np.array([[cos(theta13), -sin(theta13), 0, 0], [0, 0, -1 , 0], [-sin(theta13), cos(theta13), 0, 0], [0, 0, 0, 1]])
        
    T34 = np.array([[cos(theta14), -sin(theta14), 0, 0],[0, 0, -1, r4],[sin(theta4), cos(theta4), 0, 0],[0, 0, 0, 1]])
        
    T45 = np.array([[cos(theta15), -sin(theta15), 0, 0],[0, 0, -1, 0],[sin(theta15), cos(theta15), 0, 0],[0, 0, 0, 1]])

    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    T05 =  np.dot(T04,T45) 
    
    ##Extraction of rotation matrix
    R01 = T01[:3,:3]
    
    R02 = T02[:3,:3]
    
    R03 = T03[:3,:3]
    
    R04 = T04[:3,:3]
    
    R05 = T05[:3,:3]
    
   ####Length of segments and lengths of segments considering CoM
    
    r11 = np.array([[0.004302],[0.00738],[0.00498]])
    
    rc11 = np.array([[2.151e-3], [3.69e-3], [2.49e-3]])
   
    r22 = np.array([[0.002576],[-0.04222],[-0.00209]])
        
    rc22 = np.array([[1.288e-3], [-2.111e-2], [-1.045e-3]])
    
    r33 = np.array([[0.005438],[-7.274e-05],[0.0354]])
    
    rc33 = np.array([[2.719e-3], [-3.637e-5], [1.77e-2]])
 
    r44 = np.array([[-0.002606],[-0.005556],[-0.005246]])
    
    rc44 = np.array([[-1.303e-3], [-2.778e-3], [-2.623e-3]])
   
    r55 = np.array([[0.014608],[0.0292],[-0.0006866]])
  
    rc55 = np.array([[7.304e-3], [1.46e-2], [-3.433e-4]])
    
    ####Angular velocities and accelerations
    g = 9.81
         
    wp01 = np.array([[0],[0],[qp11]])
  
    wp02 = np.array([[0],[0],[qp11+qp12]])
    
    wp03 = np.array([[0],[0],[qp11+qp12+qp13]])
    
    wp04 = np.array([[0],[0],[qp11+qp12+qp13+qp14]])
    
    wp05 = np.array([[0],[0],[qp11+qp12+qp13+qp14+qp15]])
    
    wpp01 = np.array([[0],[0],[qpp11]])
    
    wpp02 = np.array([[0],[0],[qpp11+qpp12]])
    
    wpp03 = np.array([[0],[0],[qpp11+qpp12+qpp13]])
    
    wpp04 = np.array([[0],[0],[qpp11+qpp12+qpp13+qpp14]])
    
    wpp05 = np.array([[0],[0],[qpp11+qpp12+qpp13+qpp14+qpp15]])
    
    wp = np.concatenate((wp01,wp02,wp03,wp04,wp05),axis=0)
    
    wpp = np.concatenate((wpp01,wpp02,wpp03,wpp04,wpp05),axis=0)
    
    r = np.concatenate((r11,r22,r33,r44,r55),axis=0)
    
    rc = np.concatenate((rc11,rc22,rc33,rc44,rc55),axis=0)
    
    #1st segment
        
        #Center of graviy velocity and acceleration
      
    Vp00 = np.array([[0],[-g],[0]])
    
    rc01= R01@rc11
   
    Vc01 = np.cross(wp01,rc01,axis=0)
    
    Vpc01 = Vp00 + np.cross(wpp01,rc01,axis=0) + np.cross(wp01,Vc01,axis=0)
        
        #Link velocity and acceleration
        
    r01= R01@r11
    
    V01 =  np.cross(wp01,r01,axis=0)
    
    Vp01 =  Vp00 + np.cross(wpp01,r01,axis=0) + np.cross(wp01,V01,axis=0)
    
    #2nd segment
        #Center of graviy velocity and acceleration
   
    rc02= r01 + R02@rc22
    
    Vc02 = V01 + np.cross(wp02,rc02,axis=0)
    
    Vpc02 = Vp01 + np.cross(wpp02,rc02,axis=0) + np.cross(wp02,Vc02,axis=0)
    
        #Link velocity and acceleration
        
    r02= r01 + R02@r22
    
    V02 =  V01 + np.cross(wp02,r02,axis=0)
    
    Vp02 =  Vp01 + np.cross(wpp02,r02,axis=0) + np.cross(wp02,V02,axis=0)
    
    #3th segment
    
        #Center of graviy velocity and acceleration
   
    rc03= r02 + R03@rc33
    
    Vc03 = V02 + np.cross(wp03,rc03,axis=0)
    
    Vpc03 = Vp02 + np.cross(wpp03,rc03,axis=0) + np.cross(wp03,Vc03,axis=0)
    
        #Link velocity and acceleration
        
    r03= r02 + R03@r33
    
    V03 =  V02 + np.cross(wp03,r03,axis=0)
    
    Vp03 =  Vp02 + np.cross(wpp03,r03,axis=0) + np.cross(wp03,V03,axis=0)
    
    #4th segment
    
        #Center of graviy velocity and acceleration
   
    rc04= r03 + R04@rc44
    
    Vc04 = V03 + np.cross(wp04,rc04,axis=0)
    
    Vpc04 = Vp03 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r04= r03 + R04@r44 
    
    V04 =  V03 + np.cross(wp04,r04,axis=0)
    
    Vp04 =  Vp03 + np.cross(wpp04,r04,axis=0) + np.cross(wp04,V04,axis=0)
    
    #5th segment
    
        #Center of graviy velocity and acceleration
   
    rc05= r04 + R04@rc44
    
    Vc05 = V04 + np.cross(wp05,rc05,axis=0)
    
    Vpc05 = Vp04 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r05= r04 + R04@r44
    
    V05 =  V04 + np.cross(wp05,r05,axis=0)
    
    Vp05 =  Vp04 + np.cross(wpp05,r05,axis=0) + np.cross(wp05,V05,axis=0)
    
    # =============================================================================
    # Backward propagation
    # =============================================================================
        
    #5th segment
    
        ##Inertia matrix and link mass
        
    J5 = np.array([[0.000155, 2.323e-8, -2.471e-8],[0, 0.00075, 1.535e-6],[0, 0, 0.00069]])
    
    m5 =  2.629e-1

    F65 = np.zeros((3,3)) #Absence of force on the effector
    
    F54 = m5*Vp05
    
    R5 = rc05 - r05
    
    A5 = J5@wp05
    
    ###Torque value on 5th segment
    τ54 = -np.cross(F54,R5,axis=0) + J5@wpp05 + np.cross(wp05,A5,axis=0)
    
    #4th segment
    
        ##Inertia matrix and link mass
        
    J4 = np.array([[0.0001, -3.891e-7, 9.717e-8],[0, 8.942e-8, -1.317e-6],[0, 0, 4.262e-5]])
    
    m4 = 1.589e-1
    
    R4 = rc04 - r04
    
    r4 =-r05 -rc04
    
    A4 = J4@wp04
    
    F43 = m4*Vp04 + F54
    
    ###Torque value on 4th segment
    τ43 =   τ54 - np.cross(F43,R4,axis=0) + np.cross(F54,r4,axis=0) + J4@wpp04 + np.cross(wp04,A4,axis=0)

    #3th segment
    
        ##Inertia matrix and link mass
        
    J3 = np.array([[3.847e-5, -2.386e-10, -1.206e-9],[0, 2.732e-5, 0],[0, 1.475e-6, 3.266e-5]])
       
    m3 = 9.268e-3

    R3 = rc03 - r03
    
    r3 =-r04 -rc03
    
    A3 = J3@wp03
    
    F32 = m3*Vp03 + F43
    
    ###Torque value on 3th segment
    τ32 =   τ43 - np.cross(F32,R3,axis=0) + np.cross(F43,r3,axis=0) + J3@wpp03 + np.cross(wp03,A3,axis=0)

     
    #2nd segment
    
        ##Inertia matrix and link mass
        
    J2 = np.array([[5.936e-5, -8.257e-11, -2.55e-10],[0, 5.986e-5, 3.948e-7],[0, 0, 2.36e-5]])
       
    m2 = 3.841e-2

    R2 = rc02 - r02
    
    r2 =-r03 -rc02
    
    A2 = J2@wp02
    
    F21 = m2*Vp02 + F32
    
    ###Torque value on 2th segment
    τ21 = τ32 - np.cross(F21,R2,axis=0) + np.cross(F32,r2,axis=0) + J2@wpp02 + np.cross(wp02,A2,axis=0)
    
    #1st segment
    
        ##Inertia matrix and link mass
        
    J1 = np.array([[0.00015, -2.416e-7, -2.066e-7],[0, 0.00013, -2.139e-6],[0, 0, 6.527e-5]])
       
    m1 = 1.677e-1

    R1 = rc01 - r01
    
    r1 =-r02 -rc01
    
    A1 = J1@wp01
    
    F10 = m1*Vp01 + F21
    
    ###Torque value on 1st segment
    τ10 = τ21 - np.cross(F10,R1,axis=0) + np.cross(F21,r1,axis=0) + J1@wpp01 + np.cross(wp01,A1,axis=0)

    
    ###Matrix of torques
    
    a = np.concatenate((τ10,τ21))
     
    b = np.concatenate((a,τ32))
       
    c = np.concatenate((b,τ43))      
                   
    d = np.concatenate((c,τ54))
    
    
    return d

# =============================================================================
# left arm :  forward and backward
# =============================================================================

def left_arm(theta16, theta17, theta18, theta19, qp16, qp17, qp18, qp19, qpp16, qpp17, qpp18, qpp19):
    
    # =============================================================================
    # Forward propagation
    # =============================================================================

    ####Kinematic model
    
    a0 = 0.05
    
    a1 = 0.004
    
    a3 = 0.0185 - 0.01
    
    r1 = 0.0771
    
    r3 = 0.0284 + 0.03625 + 0.11175
    
    T01 = np.array([[-sin(theta16), -cos(theta16), 0, a0],[cos(theta16), -sin(theta16), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[-cos(theta17), sin(theta17), 0, a1],[0, 0, 1, 0],[sin(theta17), cos(theta18), 0, 0],[0, 0, 0, 1]])
    
    T23 = np.array([[sin(theta18), cos(theta18), 0, 0], [0, 0, 1, r3], [cos(theta18), -sin(theta18), 0, 0],[0, 0, 0, 1]])
    
    T34 = np.array([[sin(theta19), cos(theta19), 0, a3], [0, 0, 1, 0], [cos(theta19), -sin(theta19), 0, 0],[0, 0, 0, 1]])

    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
     
    ##Extraction of rotation matrix
    
    R01 = T01[:3,:3]
    
    R02 = T02[:3,:3]
    
    R03 = T03[:3,:3]
    
    R04 = T04[:3,:3]
    
   ####Length of segments and lengths of segments considering CoM
    
    r11 = np.array([[ 0.010006],[0.01945],[-0.0013064]])
    
    rc11 = np.array([[5.003e-3], [9.725e-3], [-6.532e-4]])
   
    r22 = np.array([[-0.0004144],[0.003326],[0.00479]])
        
    rc22 = np.array([[-2.072e-4], [1.663e-3], [2.395e-3]])
    
    r33 = np.array([[-0.0007788],[0.0017808],[0.00015656]])
    
    rc33 = np.array([[-3.894e-4], [8.904e-4], [7.828e-5]])
 
    r44 = np.array([[-0.012876],[0.009032],[0.017664]])
    
    rc44 = np.array([[-6.438e-3], [4.516e-3], [8.832e-3]])
    
    ####Angular velocities and accelerations
    g = 9.81
    
    wp01 = np.array([[0],[0],[qp16]])
  
    wp02 = np.array([[0],[0],[qp16+qp17]])
    
    wp03 = np.array([[0],[0],[qp16+qp17+qp18]])
    
    wp04 = np.array([[0],[0],[qp16+qp17+qp18+qp19]])
    
    wpp01 = np.array([[0],[0],[qpp16]])
    
    wpp02 = np.array([[0],[0],[qpp16+qpp17]])
    
    wpp03 = np.array([[0],[0],[qpp16+qpp17+qpp18]])
    
    wpp04 = np.array([[0],[0],[qpp16+qpp17+qpp18+qpp19]])
    
    #1st segment
        
        #Center of graviy velocity and acceleration
      
    Vp00 = np.array([[0],[-g],[0]])
    
    rc01= R01@rc11
   
    Vc01 = np.cross(wp01,rc01,axis=0)
    
    Vpc01 = Vp00 + np.cross(wpp01,rc01,axis=0) + np.cross(wp01,Vc01,axis=0)
        
        #Link velocity and acceleration
    
    r01= R01@r11
    
    V01 =  np.cross(wp01,r01,axis=0)
    
    Vp01 =  Vp00 + np.cross(wpp01,r01,axis=0) + np.cross(wp01,V01,axis=0)
    
    #2nd segment
        #Center of graviy velocity and acceleration
   
    rc02= r01 + R02@rc22
    
    Vc02 = V01 + np.cross(wp02,rc02,axis=0)
    
    Vpc02 = Vp01 + np.cross(wpp02,rc02,axis=0) + np.cross(wp02,Vc02,axis=0)
    
        #Link velocity and acceleration
        
    r02= r01 + R02@r22 
    
    V02 =  V01 + np.cross(wp02,r02,axis=0)
    
    Vp02 =  Vp01 + np.cross(wpp02,r02,axis=0) + np.cross(wp02,V02,axis=0)
    
    #3th segment
    
        #Center of graviy velocity and acceleration
   
    rc03= r02 + R03@rc33
    
    Vc03 = V02 + np.cross(wp03,rc03,axis=0)
    
    Vpc03 = Vp02 + np.cross(wpp03,rc03,axis=0) + np.cross(wp03,Vc03,axis=0)
    
        #Link velocity and acceleration
        
    r03= r02 + R03@r33 
    
    V03 =  V02 + np.cross(wp03,r03,axis=0)
    
    Vp03 =  Vp02 + np.cross(wpp03,r03,axis=0) + np.cross(wp03,V03,axis=0)
    
    #4th segment
    
        #Center of graviy velocity and acceleration
   
    rc04= r03 + R04@rc44
    
    Vc04 = V03 + np.cross(wp04,rc04,axis=0)
    
    Vpc04 = Vp03 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r04= r03 + R04@r44 

    V04 =  V03 + np.cross(wp04,r04,axis=0)
    
    Vp04 =  Vp03 + np.cross(wpp04,r04,axis=0) + np.cross(wp04,V04,axis=0)
    
    
    # =============================================================================
    # Backward propagation
    # =============================================================================
        
    #4th segment
    
        ##Inertia matrix and link mass
        
    J4 = np.array([[0.000185, 0, 0],[-1.356e-5, 1.51e-5, 0],[-9.008e-8, 7.317e-6, 0.000182]])
    
    m4 = 4.865e-2

    F54 = np.zeros((3,3)) #Absence of force on the effector
    
    F43 = m4*Vp04
    
    R4 = rc04 - r04
    
    A4 = J4@wp04
    
    ###Torque value on 5th segment
    τ43 = -np.cross(F43,R4,axis=0) + J4@wpp04 + np.cross(wp04,A4,axis=0)
    
    #3th segment
    
        ##Inertia matrix and link mass
        
    J3 = np.array([[0.00028, 3.11e-6, 0],[0, 4.233e-5, 0],[6.032e-7, 3.245e-9, 0.000276]])
    
    m3 = 1.681e-1
    
    R3 = rc03 - r03
    
    r3 =-r04 -rc03
    
    A3 = J3@wp03
    
    F32 = m3*Vp03 + F43
    
    ###Torque value on 4th segment
    τ32 =   τ43 - np.cross(F32,R3,axis=0) + np.cross(F43,r3,axis=0) + J3@wpp03 + np.cross(wp03,A3,axis=0)

    #2nd segment
    
        ##Inertia matrix and link mass
        
    J2 = np.array([[2.764e-5, 1.322e-10, 6.669e-10],[0, 1.644e-5, 7.234e-7],[0, 0, 2.264e-5]])
       
    m2 = 8.281e-2

    R2 = rc02 - r02
    
    r2 =-r03 -rc02
    
    A2 = J2@wp02
    
    F21 = m2*Vp02 + F32
    
    ###Torque value on 3th segment
    τ21 =   τ32 - np.cross(F21,R2,axis=0) + np.cross(F32,r2,axis=0) + J2@wpp02 + np.cross(wp02,A2,axis=0)
    
    #1st segment
    
        ##Inertia matrix and link mass
        
    J1 = np.array([[3.396e-6, 0, 0],[1.0038e-8, 3.567e-6, 0],[2.954e-8, -4.795e-8, 1.846e-6]])
       
    m1 = 8.436e-3

    R1 = rc01 - r01
    
    r1 =-r02 -rc01
    
    A1 = J1@wp01
    
    F10 = m1*Vp01 + F21
    
    τ10 = τ32 - np.cross(F10,R1,axis=0) + np.cross(F21,r1,axis=0) + J1@wpp01 + np.cross(wp01,A1,axis=0)
    
    ###Matrix of torques
    a = np.concatenate((τ10,τ21))
     
    b = np.concatenate((a,τ32))
       
    c = np.concatenate((b,τ43))      
                   
    return c
# =============================================================================
# right arm :  forward and backward
# =============================================================================

def right_arm(theta20, theta21, theta22, theta23, qp20, qp21, qp22, qp23, qpp20, qpp21, qpp22, qpp23):
    
    # =============================================================================
    # Forward propagation
    # =============================================================================

    ####Kinematic model
    a0 = 0.05
    
    a1 = 0.004
    
    a3 = 0.0185 - 0.01
    
    r1 = 0.0771
    
    r3 = 0.0284 + 0.03625 + 0.11175
    
    T01 = np.array([[-sin(theta20), -cos(theta20), 0, a0],[cos(theta20), -sin(theta20), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
    
    T12 = np.array([[-cos(theta21), sin(theta21), 0, a1],[0, 0, -1, 0],[-sin(theta21), -cos(theta21), 0, 0],[0, 0, 0, 1]])
    
    T23 = np.array([[-sin(theta22), cos(theta22), 0, 0], [0, 0, -1, -r3], [-cos(theta22), -sin(theta22), 0, 0],[0, 0, 0, 1]])
   
    T34 = np.array([[-sin(theta23), -cos(theta23), 0, a3], [0, 0, -1, 0], [cos(theta23), -sin(theta23), 0, 0],[0, 0, 0, 1]])
    
    T02 = np.dot(T01,T12)
    
    T03 = np.dot(T02,T23)
    
    T04 =  np.dot(T03,T34)
    
    ##Extraction of rotation matrix
    
    R01 = T01[:3,:3]
    
    R02 = T02[:3,:3]
    
    R03 = T03[:3,:3]
    
    R04 = T04[:3,:3]
    
   ####Length of segments and lengths of segments considering CoM
    
    r11 = np.array([[ 0.0008718],[-0.019446],[-0.0013332]])
    
    rc11 = np.array([[4.359e-4], [-9.723e-3], [-6.666e-4]])
   
    r22 = np.array([[-0.0004112],[0.002988],[0.004782]])
        
    rc22 = np.array([[-2.056e-4], [1.494e-3], [2.391e-3]])
    
    r33 = np.array([[-0.0007462],[-0.0019052],[0.00016254]])
    
    rc33 = np.array([[-3.731e-4], [-9.526e-4], [8.127e-5]])
 
    r44 = np.array([[-0.014256],[-0.012018],[0.017104]])
    
    rc44 = np.array([[-7.128e-3], [-6.009e-3], [8.552e-3]])
    
    ####Angular velocities and accelerations
    g = 9.81
    
    wp01 = np.array([[0],[0],[qp20]])
  
    wp02 = np.array([[0],[0],[qp20+qp21]])
    
    wp03 = np.array([[0],[0],[qp20+qp21+qp22]])
    
    wp04 = np.array([[0],[0],[qp20+qp21+qp22+qp23]])
    
    wpp01 = np.array([[0],[0],[qpp20]])
    
    wpp02 = np.array([[0],[0],[qpp20+qpp21]])
    
    wpp03 = np.array([[0],[0],[qpp20+qpp21+qpp22]])
    
    wpp04 = np.array([[0],[0],[qpp20+qpp21+qpp22+qpp23]])
    
    #1st segment 
        
        #Center of graviy velocity and acceleration
      
    Vp00 = np.array([[0],[-g],[0]])
    
    rc01= R01@rc11
   
    Vc01 = np.cross(wp01,rc01,axis=0)
    
    Vpc01 = Vp00 + np.cross(wpp01,rc01,axis=0) + np.cross(wp01,Vc01,axis=0)
        
        #Link velocity and acceleration
        
    r01= R01@r11
    
    V01 =  np.cross(wp01,r01,axis=0)
    
    Vp01 =  Vp00 + np.cross(wpp01,r01,axis=0) + np.cross(wp01,V01,axis=0)
    
    #2nd segment
        #Center of graviy velocity and acceleration
   
    rc02= r01 + R02@rc22
    
    Vc02 = V01 + np.cross(wp02,rc02,axis=0)
    
    Vpc02 = Vp01 + np.cross(wpp02,rc02,axis=0) + np.cross(wp02,Vc02,axis=0)
    
        #Link velocity and acceleration
        
    r02= r01 + R02@r22
    
    V02 =  V01 + np.cross(wp02,r02,axis=0)
    
    Vp02 =  Vp01 + np.cross(wpp02,r02,axis=0) + np.cross(wp02,V02,axis=0)
    
    #3th segment
    
        #Center of graviy velocity and acceleration
   
    rc03= r02 + R03@rc33
    
    Vc03 = V02 + np.cross(wp03,rc03,axis=0)
    
    Vpc03 = Vp02 + np.cross(wpp03,rc03,axis=0) + np.cross(wp03,Vc03,axis=0)
    
        #Link velocity and acceleration
        
    r03= r02 + R03@r33
    
    V03 =  V02 + np.cross(wp03,r03,axis=0)
    
    Vp03 =  Vp02 + np.cross(wpp03,r03,axis=0) + np.cross(wp03,V03,axis=0)
    
    #4th segment
    
        #Center of graviy velocity and acceleration
   
    rc04= r03 + R04@rc44
    
    Vc04 = V03 + np.cross(wp04,rc04,axis=0)
    
    Vpc04 = Vp03 + np.cross(wpp04,rc04,axis=0) + np.cross(wp04,Vc04,axis=0)
    
        #Link velocity and acceleration
        
    r04= r03 + R04@r44 
    
    V04 =  V03 + np.cross(wp04,r04,axis=0)
    
    Vp04 =  Vp03 + np.cross(wpp04,r04,axis=0) + np.cross(wp04,V04,axis=0)
    
    
    # =============================================================================
    # Backward propagation
    # =============================================================================
        
    #4th segment
    
        ##Inertia matrix and link mass
        
    J4 = np.array([[0.0001858, 9.1e-8, -7.322e-6],[0, 1.51e-5, 0],[0, -1.358e-5, 0.0001827]])
    
    m4 = 4.865e-2

    F54 = np.zeros((3,3)) #Absence of force on the effector
    
    F43 = m4*Vp04
    
    R4 = rc04 - r04
    
    A4 = J4@wp04
    
    ###Torque value on 5th segment
    τ43 = -np.cross(F43,R4,axis=0) + J4@wpp04 + np.cross(wp04,A4,axis=0)
    
    #3th segment
    
        ##Inertia matrix and link mass
        
    J3 = np.array([[0.00028, 0, 0],[3.11e-6, 4.232e-5, 0],[-3.863e-9, -6.031e-7, 0.000275]])
    
    m3 = 1.681e-1
    
    R3 = rc03 - r03
    
    r3 =-r04 -rc03
    
    A3 = J3@wp03
    
    F32 = m3*Vp03 + F43
    
    ###Torque value on 4th segment
    τ32 =   τ43 - np.cross(F32,R3,axis=0) + np.cross(F43,r3,axis=0) + J3@wpp03 + np.cross(wp03,A3,axis=0)

    #2nd segment
    
        ##Inertia matrix and link mass
        
    J2 = np.array([[2.765e-5, 0, 0],[6.689e-10, 1.644e-5, 1.322e-10],[7.234e-7, 0, 2.264e-5]])
       
    m2 = 8.281e-2

    R2 = rc02 - r02
    
    r2 =-r03 -rc02
    
    A2 = J2@wp02
    
    F21 = m2*Vp02 + F32
    
    ###Torque value on 3th segment
    τ21 =   τ32 - np.cross(F21,R2,axis=0) + np.cross(F32,r2,axis=0) + J2@wpp02 + np.cross(wp02,A2,axis=0)

    #1st segment
    
        ##Inertia matrix and link mass
        
    J1 = np.array([[3.4e-6, -3.01e-8, 4.485e-8],[0, 3.57e-6, 0],[0, 1.16e-8, 1.855e-6]])
       
    m1 = 8.436e-3

    R1 = rc01 - r01
    
    r1 =-r02 -rc01
    
    A1 = J1@wp01
    
    F10 = m1*Vp01 + F21
    
    τ10 = τ32 - np.cross(F10,R1,axis=0) + np.cross(F21,r1,axis=0) + J1@wpp01 + np.cross(wp01,A1,axis=0)
    
    ##Matrix of torques
    a = np.concatenate((τ10,τ21))
     
    b = np.concatenate((a,τ32))
       
    c = np.concatenate((b,τ43))      
                   
    return c

# =============================================================================
# Head: Forward and Backward
# =============================================================================

def head(theta24, theta25, qp24, qp25, qpp24, qpp25):
    
    # =============================================================================
    # Forward propagation
    # =============================================================================

    ####Kinematic model
     
    r1 = 0.084 + 0.0199999999999999
      
    T01 = np.array([[cos(theta24), -sin(theta24), 0, 0],[sin(theta24), cos(theta24), 0, 0],[0, 0, 1, r1],[0, 0, 0, 1]])
      
    T12 = np.array([[cos(theta25), -sin(theta25), 0, 0],[0, 0, -1, 0], [sin(theta25), cos(theta25), 0, 0],[0, 0, 0, 1]])
      
    T02 = np.dot(T01,T12)
    
    ##Extraction of rotation matrix
    
    R01 = T01[:3,:3]
    
    R02 = T02[:3,:3]
    
    ####Length of segments and lengths of segments considering CoM
     
    r11 = np.array([[5.788e-06],[-0.01293],[-0.002948]])
    
    rc11 = np.array([[2.894e-6], [-6.465e-3], [-1.474e-3]])
   
    r22 = np.array([[0.005328],[-0.008464],[-0.0017804]])
        
    rc22 = np.array([[2.664e-3], [-4.232e-3], [-8.902e-4]])
   
    ####Angular velocities and accelerations
    g = 9.81
    
    wp01 = np.array([[0],[0],[qp24]])
  
    wp02 = np.array([[0],[0],[qp24+qp25]])
    
    wpp01 = np.array([[0],[0],[qpp24]])
    
    wpp02 = np.array([[0],[0],[qpp24+qpp25]])
    
    #1st segment : 
        
        #Center of graviy velocity and acceleration
      
    Vp00 = np.array([[0],[-g],[0]])
    
    rc01= R01@rc11
   
    Vc01 = np.cross(wp01,rc01,axis=0)
    
    Vpc01 = Vp00 + np.cross(wpp01,rc01,axis=0) + np.cross(wp01,Vc01,axis=0)
        
        #Link velocity and acceleration
        
    r01= R01@r11
    
    V01 =  np.cross(wp01,r01,axis=0)
    
    Vp01 =  Vp00 + np.cross(wpp01,r01,axis=0) + np.cross(wp01,V01,axis=0)
    
    #2nd segment
        #Center of graviy velocity and acceleration
   
    rc02= r01 + R02@rc22
    
    Vc02 = V01 + np.cross(wp02,rc02,axis=0)
    
    Vpc02 = Vp01 + np.cross(wpp02,rc02,axis=0) + np.cross(wp02,Vc02,axis=0)
    
        #Link velocity and acceleration
        
    r02= r01 + R02@r22 
    
    V02 =  V01 + np.cross(wp02,r02,axis=0)
    
    Vp02 =  Vp01 + np.cross(wpp02,r02,axis=0) + np.cross(wp02,V02,axis=0)
    
    # =============================================================================
    # Backward propagation
    # =============================================================================
        
    #2th segment
    
        ##Inertia matrix and link mass
        
    J2 = np.array([[0.00035, -2.1759e-7, 4.107e-6],[0, 0.00038, -2.166e-5],[0, 0.00038, -2.166e-5]])
    
    m2 = 1.886e-1

    F21 = np.zeros((3,3)) #Absence of force on the effector
    
    F21 = m2*Vp02
    
    R2 = rc02 - r02
    
    A2 = J2@wp02
    
    ###Torque value of 2th segment
    τ21 = -np.cross(F21,R2,axis=0) + J2@wpp02 + np.cross(wp02,A2,axis=0)
    
    #1st segment
    
        ##Inertia matrix and link mass
        
    J1 = np.array([[7.26e-7, -9.3e-8, 2.014e-10],[0, 2.335e-6, 1.5538e-11],[0, 0, 2.574e-6]])
    
    m1 = 5.885e-3
    
    R1 = rc01 - r01
    
    r1 =-r02 -rc01
    
    A1 = J1@wp01
    
    F10 = m1*Vp01 + F21
    
    ###Torque value of 1st segment
    τ10 =  τ21 - np.cross(F10,R1,axis=0) + np.cross(F21,r1,axis=0) + J1@wpp01 + np.cross(wp01,A1,axis=0)
    
    ###Matrice of torques
    a = np.concatenate((τ10,τ21))
    
    return a
    
if __name__ == '__main__':
    
# =============================================================================
# 2nd part : Final torque of robot 
# =============================================================================

    theta1=symbols('theta1')
    
    theta2=symbols('theta2')
    
    theta3=symbols('theta3')
    
    theta4=symbols('theta4')
    
    theta5=symbols('theta5')
     
    theta6=symbols('theta6')
    
    theta7=symbols('theta7')
    
    theta8=symbols('theta8')
    
    theta9=symbols('theta9')
    
    theta10=symbols('theta10')
    
    theta11=symbols('theta11')
    
    theta12=symbols('theta12')
    
    theta13=symbols('theta13')
    
    theta14=symbols('theta14')
    
    theta15=symbols('theta15')
    
    theta16=symbols('theta16')
    
    theta17=symbols('theta17')
    
    theta18=symbols('theta18')
    
    theta19=symbols('theta19')
    
    theta20=symbols('theta20')
    
    theta21=symbols('theta21')
    
    theta22=symbols('theta22')
    
    theta23=symbols('theta23')
    
    theta24=symbols('theta24')
    
    theta25=symbols('theta25')
    
    qp1, qpp1 = symbols('qp1 qpp1')
    
    qp2, qpp2 = symbols('qp2 qpp2')
    
    qp3, qpp3 = symbols('qp3 qpp3')
    
    qp4, qpp4 = symbols('qp4 qpp4')
    
    qp5, qpp5 = symbols('qp5 qpp5')

    qp6, qpp6 = symbols('qp6 qpp6')
    
    qp7, qpp7 = symbols('qp7 qpp7')
    
    qp8, qpp8 = symbols('qp8 qpp8')
    
    qp9, qpp9 = symbols('qp9 qpp9')
    
    qp10, qpp10 = symbols('qp10 qpp10')
    
    qp11, qpp11 = symbols('qp11 qpp11')
    
    qp12, qpp12 = symbols('qp12 qpp12')
    
    qp13, qpp13 = symbols('qp13 qpp13')
    
    qp14, qpp14 = symbols('qp14 qpp14')
    
    qp15, qpp15 = symbols('qp15 qpp15')
    
    qp16, qpp16 = symbols('qp16 qpp16')
    
    qp17, qpp17 = symbols('qp17 qpp17')
    
    qp18, qpp18 = symbols('qp18 qpp18')
    
    qp19, qpp19 = symbols('qp19 qpp19')
    
    qp20, qpp20 = symbols('qp20 qpp20')
    
    qp21, qpp21 = symbols('qp21 qpp21')
    
    qp22, qpp22 = symbols('qp22 qpp22')
    
    qp23, qpp23 = symbols('qp23 qpp23')
    
    qp24, qpp24 = symbols('qp24 qpp24')
    
    qp25, qpp25 = symbols('qp25 qpp25')

    M_l_leg = left_leg(theta1, theta2, theta3, theta4, theta5, qp1, qp2, qp3, qp4, qp5, qpp1, qpp2, qpp3, qpp4, qpp5)
    
    M_r_leg = right_leg(theta6, theta7, theta8, theta9, theta10, qp6, qp7, qp8, qp9, qp10, qpp6, qpp7, qpp8, qpp9, qpp10)
    
    M_abdomen = abdomen(theta11, theta12, theta13, theta14, theta15, qp11, qp12, qp13, qp14, qp15, qpp11, qpp12, qpp13, qpp14, qpp15)
    
    M_l_arm = left_arm(theta16, theta17, theta18, theta19, qp16, qp17, qp18, qp19, qpp16, qpp17, qpp18, qpp19)
    
    M_r_arm = right_arm(theta20, theta21, theta22, theta23, qp20, qp21, qp22, qp23, qpp20, qpp21, qpp22, qpp23)
    
    Head = head(theta24, theta25, qp24, qp25, qpp24, qpp25)
      
    #Left leg
    tl1=(M_l_leg[0]**2 +  M_l_leg[1]**2  +  M_l_leg[2]**2 )**(1/2)
    tl2=(M_l_leg[3]**2 +  M_l_leg[4]**2  +  M_l_leg[5]**2 )**(1/2)
    tl3=(M_l_leg[6]**2 +  M_l_leg[7]**2  +  M_l_leg[8]**2 )**(1/2)
    tl4=(M_l_leg[9]**2 +  M_l_leg[10]**2  +  M_l_leg[11]**2 )**(1/2)
    t15=(M_l_leg[12]**2 +  M_l_leg[13]**2  +  M_l_leg[14]**2 )**(1/2)
    
    #Right leg
    tr1=(M_r_leg[0]**2 +  M_r_leg[1]**2  +  M_r_leg[2]**2 )**(1/2)
    tr2=(M_r_leg[3]**2 +  M_r_leg[4]**2  +  M_r_leg[5]**2 )**(1/2)
    tr3=(M_r_leg[6]**2 +  M_r_leg[7]**2  +  M_r_leg[8]**2 )**(1/2)
    tr4=(M_r_leg[9]**2 +  M_r_leg[10]**2  +  M_r_leg[11]**2 )**(1/2)
    tr5=(M_r_leg[12]**2 +  M_r_leg[13]**2  +  M_r_leg[14]**2 )**(1/2)
    
    #Abdomen
    t1=(M_abdomen[0]**2 +  M_abdomen[1]**2  +  M_abdomen[2]**2 )**(1/2)
    t2=(M_abdomen[3]**2 +  M_abdomen[4]**2  +  M_abdomen[5]**2 )**(1/2)
    t3=(M_abdomen[6]**2 +  M_abdomen[7]**2  +  M_abdomen[8]**2 )**(1/2)
    t4=(M_abdomen[9]**2 +  M_abdomen[10]**2  +  M_abdomen[11]**2 )**(1/2)
    t5=(M_abdomen[12]**2 +  M_abdomen[13]**2  +  M_abdomen[14]**2 )**(1/2)
    
    #Left arm
    tla1=(M_l_arm[0]**2 +  M_l_arm[1]**2  +  M_l_arm[2]**2 )**(1/2)
    tla2=(M_l_arm[3]**2 +  M_l_arm[4]**2  +  M_l_arm[5]**2 )**(1/2)
    tla3=(M_l_arm[6]**2 +  M_l_arm[7]**2  +  M_l_arm[8]**2 )**(1/2)
    tla4=(M_l_arm[9]**2 +  M_l_arm[10]**2  +  M_l_arm[11]**2 )**(1/2)
    
    #Right arm
    tra1=(M_r_arm[0]**2 +  M_r_arm[1]**2  +  M_r_arm[2]**2 )**(1/2)
    tra2=(M_r_arm[3]**2 +  M_r_arm[4]**2  +  M_r_arm[5]**2 )**(1/2)
    tra3=(M_r_arm[6]**2 +  M_r_arm[7]**2  +  M_r_arm[8]**2 )**(1/2)
    tra4=(M_r_arm[9]**2 +  M_r_arm[10]**2  +  M_r_arm[11]**2 )**(1/2)
    
    #Head
    th1 =(Head[0]**2 +  Head[1]**2  +  Head[2]**2 )**(1/2)
    th2 =(Head[3]**2 +  Head[4]**2  +  Head[5]**2 )**(1/2)

    #total Torque of robot 
    a = np.concatenate((tl1,tl2))
    b = np.concatenate((a,tl3))
    c = np.concatenate((b,tl4))
    d = np.concatenate((c,t15))
    e = np.concatenate((d,tr1)) 
    f = np.concatenate((e,tr2))
    g = np.concatenate((f,tr3))
    h = np.concatenate((g,tr4)) 
    i = np.concatenate((h,tr5))
    j = np.concatenate((i,t1))
    k = np.concatenate((j,t2))
    l = np.concatenate((k,t3))
    m = np.concatenate((l,t4))
    n = np.concatenate((m,t5))
    o = np.concatenate((n,tla1))
    p = np.concatenate((o,tla2))
    q = np.concatenate((p,tla3))
    r = np.concatenate((q,tla4))
    s = np.concatenate((r,tra1))
    t = np.concatenate((s,tra2))
    u = np.concatenate((t,tra3))
    v = np.concatenate((u,tra4))
    w = np.concatenate((v,th1))
    τ = np.concatenate((w,th2))
    
    τ  =  τ.reshape((25,1))
    
  

  


    
